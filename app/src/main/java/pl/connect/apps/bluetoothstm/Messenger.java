package pl.connect.apps.bluetoothstm;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Messenger extends AppCompatActivity {

    private Button sendButton;
    private ListView messages;
    private List<String> messagesList = new ArrayList<>();
    private AcceptThread at = null;
    private ConnectThread ct = null;
    private static final String TAG = "HABBA";
    private ArrayAdapter<String> itemsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainMenu.TYPE);
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        messages = findViewById(R.id.messages);
        itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, messagesList);
        messages.setAdapter(itemsAdapter);

        @SuppressLint("HandlerLeak") Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Log.d(TAG, "Message Handled");
                if(message != null)
                    Messenger.this.runOnUiThread(() -> refresh("< " + msg.getData().getString("what")));
            }
        };

        if(message != null) {
            if (message.equals(MainMenu.SERVER)) {
                at = new AcceptThread(mBluetoothAdapter, MainMenu.UUID, handler);
                at.start();
            } else if (message.equals(MainMenu.CLIENT)) {
                BluetoothDevice target = intent.getParcelableExtra("TARGET");
                if (target != null) {
                    ct = new ConnectThread(target, mBluetoothAdapter, MainMenu.UUID, handler);
                    ct.start();
                }
            }
        }

        EditText editText = findViewById(R.id.message);
        sendButton = findViewById(R.id.send_button);
        sendButton.setOnClickListener((view ->{
            if(ct != null)
                ct.write(editText.getText().toString());
            else if (at != null)
                at.write(editText.getText().toString());

            refresh("> " + editText.getText().toString());
            }
        ));


    }

    void refresh(String message){
        messagesList.add(message);
        itemsAdapter.notifyDataSetChanged();
    }
}
