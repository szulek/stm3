package pl.connect.apps.bluetoothstm;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

public class ConnectThread extends Thread {
    private static final String TAG = "HABBA";
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private BluetoothAdapter mbluetoothAdapter;
    private UUID myUuid;
    private ConnectedThread connectedThread;
    private final Handler handler;

    public ConnectThread(BluetoothDevice device, BluetoothAdapter bluetoothAdapter, UUID uuid, Handler handler) {
        BluetoothSocket tmp = null;
        mmDevice = device;
        mbluetoothAdapter = bluetoothAdapter;
        myUuid = uuid;
        this.handler = handler;

        try {
            tmp = device.createRfcommSocketToServiceRecord(myUuid);
        } catch (IOException e) {
            Log.e(TAG, "Socket's create() method failed", e);
        }
        mmSocket = tmp;
    }

    public void run() {
        mbluetoothAdapter.cancelDiscovery();

        try {
            mmSocket.connect();
        } catch (IOException connectException) {
            try {
                mmSocket.close();
            } catch (IOException closeException) {
                Log.e(TAG, "Could not close the client socket", closeException);
            }
            return;
        }
        manageMyConnectedSocket(mmSocket);
    }

    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Could not close the client socket", e);
        }
    }

    private void manageMyConnectedSocket(BluetoothSocket socket){
        connectedThread = new ConnectedThread(socket, handler);
        connectedThread.run();
    }

    public void write(String message){
       connectedThread.write(message.getBytes());
    }
}