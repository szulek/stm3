package pl.connect.apps.bluetoothstm;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.UUID;

public class MainMenu extends AppCompatActivity {

    public static String TYPE = "TYPE";
    public static String SERVER = "server";
    public static String CLIENT = "client";
    public static UUID UUID = new UUID(123456, 123456);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        Button serverButton = findViewById(R.id.server_button);
        Button clientButton = findViewById(R.id.client_button);

        serverButton.setOnClickListener((View v) -> {
            Intent intent = new Intent(this, Messenger.class);
            intent.putExtra(TYPE, SERVER);
            startActivity(intent);
        });

        clientButton.setOnClickListener((View v) -> {
            Intent intent = new Intent(this, ConnectionChooser.class);
            intent.putExtra(TYPE, CLIENT);
            startActivity(intent);
        });
    }
}
