package pl.connect.apps.bluetoothstm;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ConnectionChooser extends AppCompatActivity {

    private static final String TAG = "HABBA";
    BluetoothDevice targetDevice = null;
    Set<BluetoothDevice> pairedDevices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_chooser);
        Intent thisIntent = getIntent();
        String message = thisIntent.getStringExtra(MainMenu.TYPE);
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        Button connectButton = findViewById(R.id.connect_button);
        connectButton.setOnClickListener((View v) -> {
            if(targetDevice != null){
                Intent intent = new Intent(this, Messenger.class);
                intent.putExtra(MainMenu.TYPE, message);
                intent.putExtra("TARGET", targetDevice);
                startActivity(intent);
            }
        });

        if (mBluetoothAdapter == null) {
            Log.e(TAG, "No Bluetooth Adapter");
        }
        else if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }

        if(mBluetoothAdapter != null){
            Spinner pairedDevicesSpinner =  findViewById(R.id.spinner);
            pairedDevices = mBluetoothAdapter.getBondedDevices();
            List<String> pairedDevicesList = new ArrayList<>();
            for(BluetoothDevice bd : pairedDevices){
                pairedDevicesList.add(bd.getName() + "[" + bd.getAddress() + "]");
            }
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, pairedDevicesList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            pairedDevicesSpinner.setAdapter(adapter);

            pairedDevicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    String targetDeviceDesc = arg0.getItemAtPosition(arg2).toString();
                    String mac = targetDeviceDesc.substring(targetDeviceDesc.indexOf('[') +1, targetDeviceDesc.indexOf(']'));
                    for (BluetoothDevice device : pairedDevices) {
                        if(device.getAddress().equals(mac))
                            targetDevice = device;
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> arg0) {}
            });

        }
    }
}
